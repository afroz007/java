import java.util.Arrays;

public class MinTPlatforms {
	static int findMPlatform(int arrival[], int departure[], int n) {

		Arrays.sort(arrival);
		Arrays.sort(departure);

		int platf = 1, result = 1;
		int i = 1, j = 0;

		while (i < n && j < n) {

			if (arrival[i] <= departure[j]) {
				platf++;
				i++;

				if (platf > result)
					result = platf;
			}

			else {
				platf--;
				j++;
			}
		}

		return result;
	}

	public static void main(String[] args) {
		int arrival[] ={900,940,950,1030,1150};
		int departure[] ={910,1000,1020,1050,1300};
		int n = arrival.length;
		System.out.println("Numbers of Platforms:" + findMPlatform(arrival, departure, n));
	}
}